package com.hw.db.controllers;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hw.db.models.Thread;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;

import javax.websocket.server.PathParam;
import static org.junit.Assert.assertNotNull;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

 class threadControllerTests {
    private User loggedIn;
    private Forum toCreate;

    @BeforeEach
    @DisplayName("thread creation test")
    void createthreadTest() {
        newId=21
        newThread = new Thread("somename", Timestamp.valueOf("2022-02-21 00:00:00"), "new forum", "newpost", "theslug", "threadname", 21);
        controller = new threadController();
        User newuser = new User("somename","n.tupikina@innopolis.ru","Natasha User","");
        testThread.setId(newId);

    }

    @Test
    @DisplayName("Correct slug creation test")
    void correctlyCreatesSlug() {
        try (Mocked<ThreadDAO> threadMock = Mockito.mock(ThreadDAO.class)) {
            doThrow("threadname").when(() -> ThreadDAO.getThreadBySlug("theslug"));
            doThrow("threadname").when(() -> ThreadDAO.getThreadById(newId));

            assert.assertNotNull(controller.correctlyCreatesSlug("theslug"));
            assert.assertNotNull(newId);
        }

    }

    @Test
    @DisplayName("Correct post creation test")
    void correctlyCreatesPost() {
        try (Mocked<ThreadDAO> threadMock = Mockito.mock(ThreadDAO.class)) {
            doAnswer((Answer<Void>) invocation -> null).when(() -> ThreadDAO.createPost(Mockito.any(), Mockito.any(), Mockito.any()));

            try (Mocked<UserDAO> usedMock = Mockito.mock(UserDAO.class)) {
                doReturn(newuser).when(() -> UserDAO.Info(Mockito.any()));

                postcreated=controller.createPost("theslug", "newpost");

                assert.assertNotNull(postcreated);
            }
        }
    }

    @Test
    @DisplayName("Editing test")
    void correctChangeTest() {

        Mocked<ThreadDAO> threadDAO = Mockito.mock(ThreadDAO.class);
        doReturn(newThread).when(() -> ThreadDAO.getThreadById(Mockito.any(Integer.class)));
        doReturn(newThread).when(() -> ThreadDAO.getThreadBySlug(Mockito.any(String.class)));

        Thread newThreadName = Mockito.mock(Thread.class);
        ResponseEntity postedited = new threadController().change("test", newThreadName);

        postedited.then().assertThat().body(HttpStatus.is2xxSuccessful(), postedited.getStatusCode());
        assertThat(newThread,  is(equalTo(postedited.getBody())));
    }

    @Test
    @DisplayName("Details test")
    void threadDetails() {
        try (Mocked<ThreadDAO> threadMock = Mockito.mock(ThreadDAO.class)) {
            doReturn(newThread).when(() -> ThreadDAO.getThreadBySlug("theslug"));
            ResponseEntity fordetails;
            assertThat(fordetails.status(HttpStatus.is2xxSuccessful()).body(newThread),  is(equalTo(controller.info("theslug"))));
            assertThat(fordetails.status(HttpStatus.is4xxClientError()).body(null).getStatusCode(),  is(equalTo(controller.info("shouldreturn4xxerror").getStatusCode())));
        }
    }

    @Test
    @DisplayName("Voting test")
    public void createVoteTest() {
        try (Mocked<ThreadDAO> threadDao = Mockito.mock(ThreadDAO.class)) {
            try (Mocked<UserDAO> userDao = Mockito.mock(UserDAO.class)) {
                doReturn(newuser).when(() -> UserDAO.Info("somename"));
                Vote votingent = new Vote("somename", newId);
                doReturn(newThread).when(() -> ThreadDAO.getThreadBySlug("theslug"));
                doReturn(thread.getVotes()).when(() -> ThreadDAO.change(vote, thread.getVotes()));
                ResponseEntity forvoting;
                assertThat(forvoting.status(HttpStatus.is2xxSuccessful()).body(newThread),  is(equalTo(controller.createVote("theslug", votingent)));
            }
        }
    }



}
